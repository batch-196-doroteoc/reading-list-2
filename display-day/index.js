// console.log("Display Day");

function displayDate(){
	let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	let d = new Date();
	let month = months[d.getMonth()];
	alert(`Today is ${month} ${d.getDate()}, ${d.getFullYear()}`);
};

displayDate();
//show date today as an alert

function displayDayandColor(){
	let daysOfWeek = ["sunday","monday","tuesday","wednesday","thursday","friday","saturday"];
	let day = new Date();
	let currentDay = daysOfWeek[day.getDay()];
		switch(currentDay) {
	case 'monday':
		 alert("Today is Monday! The color of the day is White!");
		 break;
	case 'tuesday':
		 alert("The color of the day is orange.");
		 break;
	case 'wednesday':
		 alert("The color of the day is yellow.");
		 break;
    case 'thursday':
    	 alert("One more day! Wear green.");
    	 break;
    case 'friday':
    	 alert("End of the week! Wear violet.");
    	 break;
    case 'saturday':
    	 alert("Happy Weekend and wear black!");
    	 break;
    case 'sunday':
	 	 alert("Relax take it easy and wear white");
	 	 break;
	default:
		 console.log("Please enter a valid day");
		 break;
		};
};
displayDayandColor();
